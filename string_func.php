<?php
$str = "<© W3Sçh°°¦§>";
echo htmlentities($str);
echo '<br>';
?>

<?php
$arr = array('Hello','World!','Beautiful','Day!');
echo implode(" ",$arr);
?>

<?php

// trim function
$str = "Hello World!";
echo $str . "<br>";
echo trim($str,"Hed!");
echo '<br>';
?>


<?php
echo '<br>';
echo nl2br("One line.\nAnother line.");
echo '<br>';
?>

<?php

// str_pad
$str = "Hello World";
echo str_pad($str,20,".");
echo '<br>';
?>

<?php
echo str_repeat(".",13);
echo '<br>';
?>

<?php
echo str_replace("world","Ananda","Hello world!");
echo '<br>';
?>

<?php
print_r(str_split("Hello"));
echo '<br>';
?>

<?php
echo strip_tags("Hello <b>world!</b>");
echo '<br>';
?>

<?php
echo strlen("Hello");
echo '<br>';
?>
<?php
echo strtolower("Hello WORLD.");
echo '<br>';
?>

<?php
echo strtoupper("Hello WORLD!");
echo '<br>';
?>

<?php
echo substr_compare("Hello world","Hello world",0);
echo '<br>';
?>

<?php
echo substr_count("Hello world. The world is nice","world");
echo '<br>';
?>

<?php
echo substr_replace("Hello","world",0);
echo '<br>';
?>

<?php
echo ucfirst("hello world!");
echo '<br>';
?>

<?php
$a=array('ananda','bd','24');
$b=implode(' ',$a);
var_dump($b);
echo '<br>';


?>

<?php
$a=('ananda,bd,hello');
$b=explode(',',$a);
print_r($b);
echo '<br>';

?>

<?php
$str = addcslashes("Hello World!","W");
echo($str);
?>

<?php
echo '<br>';
$a=htmlentities('<br> is a html tag');

echo $a;

?>

